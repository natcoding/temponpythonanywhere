#coding=utf-8
from django.db import models
from django.forms import ModelForm, Textarea
from django import forms
# Create your models here.

# # ================ Class currency ===================
class Currency(models.Model):
    name = models.CharField("Monnaie", max_length=24, blank=False, null=False)
    symbol = models.CharField("Symbole", max_length=4, blank=False, null=False)


class CurrencyForm(ModelForm):
    class Meta:
        model = Currency
        fields = ['name', 'symbol']


# # ================ Class Zone ===================
class Zone(models.Model):
    name = models.CharField("Nom de la Zone", max_length=25, blank=False, null=False)
    currency = models.ForeignKey(Currency)
    balance = models.FloatField("Balance de la zone", null=False, blank=False, default=0.0)

# # ================ Class Branch ===================
class Branch(models.Model):
    name = models.CharField("Nom", max_length=100, null=False, blank=False)
    passPhrase = models.CharField("Mot de passe", max_length=100, null=False, blank=False)
    current_balance = models.FloatField("Balance de la branche", null=False, blank=False, default=0.0)

    all_currencies = Currency.objects.all()
    choice_list = []
    for cur in all_currencies:
        choice_list.append((cur.symbol, cur.name))

    currency = models.CharField('Monnaie principal', max_length=40, choices=choice_list, null=False, blank=False)
    isLoggedIn = models.BooleanField("Status de connection", null=False, default=False)
    isFrozen = models.BooleanField("Etat", null=False, default=False)
    zone = models.ForeignKey(Zone, default=1)

class BranchForm(ModelForm):
    passPhrase = forms.CharField(widget=forms.PasswordInput(attrs={'size':'40', 'autocomplete':'off'}))

    class Meta:
        model = Branch
        fields = ['name', 'passPhrase']


# # ================ Class Department ===================
class Department(models.Model):
    name = models.CharField("Nom", max_length=100, null=False, blank=False)


# # ================ Class Service ===================
class Service(models.Model):
    name = models.CharField("Nom", max_length=100, null=False, blank=False)
    service_fav_icon = models.CharField("service_icon", max_length=100, null=False, blank=False)
    department = models.ForeignKey(Department)
    isFrozen = models.BooleanField("Etat", null=False, default=False)
    viewName = models.CharField("Nom de la page", max_length=100, null=False, blank=False)


# # ================ Class Agent ===================
class Agent(models.Model):
    surname = models.CharField("Nom", max_length=100, null=False, blank=True)
    names = models.CharField("Postnom et prenom", max_length=100, null=False, blank=True)
    birthdate = models.DateField("Date de naissance", null=True)
    branch = models.ForeignKey(Branch, null=True, blank=True, editable=True)
    contactNumber = models.CharField("Numero de contact", max_length=100, null=False, blank=True)
    email = models.EmailField("Email", null=True, blank=True)
    salary = models.FloatField("Balance de la zone", null=False, blank=False, default=0.0)

    tag = models.CharField("Tag", max_length=100, null=False, blank=False, editable=True)
    passPhrase = models.CharField("Mot de passe", max_length=100, null=False, blank=False)
    canLogIn = models.BooleanField("Access?", null=False, default=True)
    isLoggedIn = models.BooleanField("Connecte?", null=False, default=False)

    # class Meta:
    #     unique_together = ["tag", "passPhrase"]

class AgentForm(ModelForm):
    passPhrase = forms.CharField(widget=forms.PasswordInput(attrs={'size':'40', 'autocomplete':'off'}))

    class Meta:
        model = Agent
        fields = ['tag', 'passPhrase']

# # ================ Class Privilege ===================
class Privilege(models.Model):
    agent = models.ForeignKey(Agent)
    service = models.ForeignKey(Service)
    is_privilege_active = models.BooleanField("Etat du privilege", null=False, default=True)


# # ================ Class Client ===================
class Client(models.Model):
    surname = models.CharField("Nom", max_length=100, null=False, blank=True)
    names = models.CharField("Postnom et prenom", max_length=100, null=False, blank=True)
    birthdate = models.DateField("Date de naissance", null=True)
    branch = models.ForeignKey(Branch, blank=True, editable=True)
    contactNumber = models.CharField("Numero de contact", max_length=100, null=False, blank=True)
    email = models.EmailField("Email", null=True, blank=True)
    bank_name = models.CharField("Nom de la banque", max_length=100, null=True, blank=True)
    bank_account_number = models.CharField("Numero de compte bancaire", max_length=100, null=True, blank=True)
    worth = models.FloatField("Valeur du client", null=False, blank=False, default=0.0)
    fidelity_card_number = models.CharField(max_length=6,null=True, default=None)
    class Meta:
        unique_together = ["contactNumber", "surname"]



# class ClientFormVerification(forms.ModelForm):
#     class Meta:
#         model = Client
#         fields = ['contactNumber']
#         widgets = {'contactNumber': forms.TextInput(attrs={'class': "form-control col-md-7 col-xs-12"})}

# # ================ Class cost_percentage ===================
class Cost_Percentage(models.Model):
    item = models.CharField("Element", max_length=100, blank=False, null=False)
    percentage = models.FloatField("Cout(%)", blank=False, null=False)

    def __str__(self):
        return self.item



class Cost_PercentageForm(ModelForm):
    class Meta:
        model = Cost_Percentage
        fields = ['item','percentage']


# # ================ Class rates ===================

class Rates(models.Model):
    # get all currencies
    all_currencies = Currency.objects.all()
    choice_list = []
    for cur in all_currencies:
        choice_list.append((cur.symbol, cur.name))

    for x in choice_list:
        print(x)

    currency_from = models.CharField('Monnaie a convertir', max_length=4, choices=choice_list)
    currency_to = models.CharField('Monnaie ou convertir', max_length=4, choices=choice_list)
    value = models.FloatField("Taux", blank=False, null=False)

class RatesForm(ModelForm):
    class Meta:
        model = Rates
        fields = ['currency_from','currency_to','value']



# # ================ Class Balance_Record ===================
class Balance_Record(models.Model):
    date = models.DateField(auto_now=True)
    record = models.CharField("record", max_length=1999, null=False, blank=False)




# # ================ Class Notification ===================
class Notification(models.Model):
    Date = models.DateTimeField(auto_now=True)
    Title = models.CharField("Titre", max_length=100, blank=False, null=False)
    Author = models.ForeignKey(Branch, blank=True, editable=True, related_name='Author')
    Recipient = models.ForeignKey(Branch, blank=True, editable=True, related_name='Recipient')
    Content = models.CharField("Contenu", max_length=300, blank=False, null=False)
    Alibi = models.CharField("Cause", max_length=100, blank=False, null=False)
    isRead = models.BooleanField("Lue", null=False, default=False)



# # ================ Class Transaction ===================
class Transaction(models.Model):
    type = models.CharField("Type de transaction", max_length=100, null=False, blank=False)
    time = models.DateTimeField(auto_now=True)
    author = models.ForeignKey(Branch)
    author_agent = models.ForeignKey(Agent, null=True)

# # ================ Class Refueling ===================
class Refueling(models.Model):
    date = models.DateTimeField(auto_now=True)
    amount = models.FloatField("Amount to refuel", null=False, blank=False, default=0.0)
    branch_to = models.ForeignKey(Branch, blank=True, editable=True)
    transactionDetails = models.ForeignKey(Transaction, null=True)


# # ================ Class Admin_Expense ===================
class Admin_Expense(models.Model):
    transactionDetails = models.ForeignKey(Transaction, null=True)
    date = models.DateTimeField(auto_now=False)
    amount = models.FloatField("Montant", null=False, blank=False)
    transactionDetails = models.ForeignKey(Transaction)
    reason = models.CharField("Motif", max_length=250, blank=False, null=False)
    branch = models.ForeignKey(Branch)

# # ================ Class Expense ===================
class Expense(models.Model):
    type = models.CharField("Type de depense", max_length=100, null=False, blank=False)
    amount = models.FloatField("Montant a envoyer", null=False, blank=False)
    transactionDetails = models.ForeignKey(Transaction)

# # ================ Class Transfer ===================
class Transfer_Computation(models.Model):
    id = models.AutoField(primary_key=True)
    amount = models.FloatField("Montant a envoyer", null=False, blank=False)

    all_currencies = Currency.objects.all()
    choice_list = []
    for cur in all_currencies:
        choice_list.append((cur.symbol, cur.name))
    currency_from = models.CharField('Monnaie', max_length=40, choices=choice_list, null=False, blank=False)

    all_branches = Branch.objects.all()
    choice_branch = []
    for branch in all_branches:
        if branch.name == 'arsene' or branch.name == 'nat_development':
            pass
        else:
            choice_branch.append((branch.name, branch.name))

    branch_to = models.CharField('Branch du destinataire', max_length=30, choices=choice_branch, null=True, blank=True, default=None)

    all_Cost_Percentage = Cost_Percentage.objects.all()

    applied_costs = models.ManyToManyField(Cost_Percentage, null=True, blank=True)
    list_applied_costs = models.CharField('Cout appliques', max_length = 200, null=True, blank=True)

    total_costs_percentage = models.FloatField('Total % des couts additionel', null=True, blank=True)

    total_to_pay = models.FloatField('Cout total de la transaction', null=True, blank=True)

    total_if_rate = models.FloatField('Cout total converti', null=True, blank=True)

    zone_to = models.IntegerField('Zone id du destinataire', null=True, blank=True)




class Transfer(models.Model):
    transactionDetails = models.ForeignKey(Transaction)

    computationDetails = models.ForeignKey(Transfer_Computation)

    receiver = models.ForeignKey(Client, related_name='Expediteur')

    sender = models.ForeignKey(Client, related_name='Destinataire')

    isCollected = models.BooleanField("Status", null=False, default=False)
    reference = models.CharField("Reference", max_length=100, null=False, blank=False, editable=False)




