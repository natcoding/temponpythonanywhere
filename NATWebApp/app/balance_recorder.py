from app.models import *

# The following methd is called by the pythonanywhere task scheduler in order to record the baance
# of each branch at the end of the day
# Also, it is this same srcipt file whic is used to freez every branch at the end of the day.

branch_list = Branch.objects.exclude(id = 1)

string_record = ''

for branch in branch_list:
    string_record = string_record + ',' + '(' + str(branch.id) + ':' + str(branch.current_balance) + ')'
    branch.isFrozen = True
    branch.save()

new_balance_record = Balance_Record(record=string_record)
new_balance_record.save()
