# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2018-02-26 10:56
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0015_transfer_computation_zone_to'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='fidelity_card_number',
            field=models.IntegerField(default=None, null=True),
        ),
    ]
