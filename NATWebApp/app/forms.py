"""
Definition of forms.
"""

from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.utils.translation import ugettext_lazy as _
from .models import Transfer_Computation, Currency

class Transfer_ComputationForm(forms.ModelForm):


    class Meta:
        model = Transfer_Computation
        exclude = ('id', 'total_if_rate','total_to_pay','total_costs_percentage', 'list_applied_costs', 'currency_from', 'zone_to')

        all_currencies = Currency.objects.all()
        choice_list = []
        for cur in all_currencies:
            choice_list.append((cur.symbol, cur.name))

        currency_from = forms.CharField(max_length=3,
                widget=forms.Select(choices=choice_list))

        applied_costs = forms.ModelMultipleChoiceField(queryset=Transfer_Computation.objects.all())
        # branch_to = froms.Multi
        widgets = {
                    'applied_costs':  forms.CheckboxSelectMultiple()
                }



class BootstrapAuthenticationForm(AuthenticationForm):
    """Authentication form which uses boostrap CSS."""
    username = forms.CharField(max_length=254,
                              widget=forms.TextInput({
                                  'class': 'form-control',
                                  'placeholder': 'User name'}))
    password = forms.CharField(label=_("Password"),
                              widget=forms.PasswordInput({
                                  'class': 'form-control',
                                  'placeholder':'Password'}))



