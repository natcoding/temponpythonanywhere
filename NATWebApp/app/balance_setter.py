from app.models import *
from datetime import datetime
from dateutil import parser

x = 2

while x < 9:
    branch = Branch.objects.get(id = x)
    date_apocalypse = parser.parse("Oct 25 2017 10:59AM") # Date where me and Arsene resetted all the branches balance to 0


    branch.current_balance = 0.0;


    #Now we treat the transfers first.
    for transaction in Transaction.objects.filter(type='Transfer').filter(time__gte= date_apocalypse ):
        #get the transfer, get its computation and substract the amount to the balance
        transfer = Transfer.objects.get(transactionDetails_id = transaction.id)
        computation = Transfer_Computation.objects.get(id=transfer.computationDetails_id)

        #We treat the two cases:
        if transaction.author_id == branch.id:
            branch.current_balance += computation.total_to_pay

        if computation.branch_to == branch.name and transfer.isCollected == True:
            branch.current_balance -= computation.total_if_rate



    #Now we treat the refuelings.
    for transaction in Transaction.objects.filter(type='Ravitaillement').filter(time__gte= date_apocalypse ):
        refueling = Refueling.objects.get(transactionDetails_id = transaction.id)
        if branch.id == refueling.branch_to_id:
            branch.current_balance += refueling.amount


    #Now we treat the expenses
    for transaction in Transaction.objects.filter(type='Expense').filter(time__gte= date_apocalypse ):
        expense = Expense.objects.get(transactionDetails_id = transaction.id)
        if branch.id == transaction.author_id:
            branch.current_balance -= expense.amount


    branch.save()
    x = x + 1

