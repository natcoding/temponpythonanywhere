from app.models import *
from pyvirtualdisplay import Display
from selenium import webdriver
from sys import stderr

rate = Rates.objects.get(id = 2)
new_rate = rate.value

with Display():
    # we can now start Firefox and it will run inside the virtual display
    browser = webdriver.Firefox()
    for rate in Rates.objects.all():
        print >> stderr, rate.currency_from + ' to ' + rate.currency_to
        search_parameter = rate.currency_from + '%20to%20' + rate.currency_to
        try:
            # Manually by Arsene
            if search_parameter != 'ZAR%20to%20EUR':
                browser.get('https://www.google.com.np/search?q=' + search_parameter)
                elem = browser.find_element_by_id("knowledge-currency__tgt-amount")
                new_rate = float(elem.text)
                print >> stderr, "Collected Value: " + str(new_rate)
                if search_parameter == 'EUR%20to%20ZAR':
                    new_rate = new_rate - 0.87
                print >> stderr, "Final Value: " + str(new_rate)
                rate.value = new_rate
                rate.save()
        except:
            print >> stderr, "Collection Failed"
        print >> stderr, "============="
    browser.quit()

for rate in Rates.objects.all():
    print >> stderr, "===="