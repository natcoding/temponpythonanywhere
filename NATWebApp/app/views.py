# coding=utf-8
"""
Definition of views.
"""
from __future__ import division
from django.shortcuts import render, render_to_response, HttpResponseRedirect, HttpResponse, redirect, get_object_or_404
from django.http import HttpRequest
from datetime import datetime, timedelta
from app.models import *
from django.contrib import messages
from django.core.urlresolvers import reverse
from .forms import Transfer_ComputationForm
import random
import time
from django.db import IntegrityError
from calendar import monthrange
from dateutil import parser
import json
from django.core.serializers.json import DjangoJSONEncoder
from django.template import Template, Context


def fidel_client(request, br_id):
    fidel_clients_list = []
    branches = Branch.objects.exclude(id= 1).exclude(id = 8 )
    if br_id == '2':
        date_begin = parser.parse("Jan 01 2018 00:00AM")
        date_end = parser.parse("Apr 30 2018 12:59PM")
    elif br_id == '3':
        date_begin = parser.parse("Oct 01 2017 00:00AM")
        date_end = parser.parse("Jan 31 2017 12:59PM")
    else:
        date_begin = parser.parse("Oct 01 2017 00:00AM")
        date_end = parser.parse("Apr 01 2018 00:00AM")

    for client in Client.objects.filter(branch_id = br_id):
        transfers = []
        for transfer in Transfer.objects.filter(receiver_id = client.id, transactionDetails__time__gte = date_begin)|Transfer.objects.filter(sender_id = client.id, transactionDetails__time__gte = date_end):
            transfers.append(transfer)
        print('{0} {1} : {2} Transfers'.format(client.surname, client.names, len(transfers)))
        if len(transfers) >= 3:
            #sorted( birthday_clients, key=lambda x: x[2], reverse=True)
            names = client.names.split(' ')
            initials = ""

            try:
                for x in names:
                    initials += x[0].upper()
                fidel_clients_list.append([client.surname, initials, len(transfers), client.fidelity_card_number, client.id])
            except:
                pass

    fidel_clients_list = sorted(fidel_clients_list, key=lambda x: x[2], reverse=True)

    total = len(fidel_clients_list)

    t = Template("<html><head><title>List des plus fidèles clients</title></head><body>{% for br in branches %}<a href='{% url 'wizzard' br.id %}'>{{ br.name }}</a> || {% endfor %}<h2>Listes des clients fidèles(Branche de {{ current_branch.name }}) <small>Periode: {{ date_begin }} - {{ date_end }}</small></h2><ul>{% for f in fidel_clients_list %}<li> <a href=\"{% url 'client_details' f.4 %}\" target=\"_blank\">{{f.0}}</a> {{f.1}} {{f.3}}</li> {% endfor %}</ul></body></html>")
    html = t.render(Context({'fidel_clients_list': fidel_clients_list, "branches":branches,"date_end":date_end, "date_begin":date_begin, "current_branch": Branch.objects.get(pk = br_id)}))
    return HttpResponse(html)


def from_fidelity_fetecher(request, card_id):
    card_author = None
    #get the CVC number from the supplied:
    cvc = card_id[12:]

    digits_A_len = int(cvc[:3])
    digits_B_len = int(cvc[3])

    #get the number of char to consider from the card_id:
    digits_len = 0
    for char in cvc:
        digits_len += int(char)
    important_digits = card_id[:digits_len]
    digits_B = int(important_digits[digits_A_len:])

    id_list = []
    #assert False
    for branch in Branch.objects.exclude(id=1):
        id = digits_B / branch.id

        if digits_B % branch.id == 0 and Client.objects.filter(pk = int(id)).exists():
            id_list.append(Client.objects.get(pk = int(id)))

    temp_id = []
    for i in id_list:
        temp_id.append(i.id)

    candidate = None
    for client in id_list:
        if client.fidelity_card_number == card_id:
            candidate = client

    if candidate != None:
        card_author = candidate
        return HttpResponse(card_author.names + ' ' + card_author.surname + ' ' + card_author.contactNumber )
    else:
        return HttpResponse('Invalid Card Number')

def fidelitycard_ID_builder(request):
    number_of_new_fidelity_created = 0
    for client in Client.objects.all():
        number_of_new_fidelity_created += 1
        if client.fidelity_card_number == None:
            id_part_one = str(client.branch_id)
            if len(id_part_one) < 2:
                id_part_one = '0' + id_part_one


            id_part_two = str(client.id)
            while len(id_part_two) < 4:
                id_part_two = '0' + id_part_two


            card_id = id_part_one + id_part_two
            client.fidelity_card_number = card_id
            client.save()
    # total_cape = 0.0
    # total_jozzy = 0.0
    # total_preto = 0.0
    # total_vaal = 0.0
    # date_begin = parser.parse("Sep 01 2017 00:00AM")
    # date_end = parser.parse("Sep 30 2017 11:59PM")
    # for transfer in Transfer.objects.filter(computationDetails__branch_to = 'vaal s.a').filter(transactionDetails__time__gte = date_begin).filter(transactionDetails__time__lte = date_end):
    #     total_vaal += Transfer_Computation.objects.get(id = transfer.computationDetails_id).total_if_rate

    # for transfer in Transfer.objects.filter(computationDetails__branch_to = 'johannesburg s.a').filter(transactionDetails__time__gte = date_begin).filter(transactionDetails__time__lte = date_end):
    #     total_jozzy += Transfer_Computation.objects.get(id = transfer.computationDetails_id).total_if_rate

    # for transfer in Transfer.objects.filter(computationDetails__branch_to = 'pretoria s.a').filter(transactionDetails__time__gte = date_begin).filter(transactionDetails__time__lte = date_end):
    #     total_preto += Transfer_Computation.objects.get(id = transfer.computationDetails_id).total_if_rate

    # for transfer in Transfer.objects.filter(computationDetails__branch_to = 'capetown s.a').filter(transactionDetails__time__gte = date_begin).filter(transactionDetails__time__lte = date_end):
    #     total_cape += Transfer_Computation.objects.get(id = transfer.computationDetails_id).total_if_rate

    # for ref in Refueling.objects.filter(date__gte = date_begin).filter(date__lte = date_end):
    #     if ref.branch_to_id == 6:
    #         total_vaal += ref.amount
    #     elif ref.branch_to_id == 7:
    #         total_cape += ref.amount
    #     elif ref.branch_to_id == 5:
    #         total_jozzy += ref.amount
    #     elif ref.branch_to_id == 4:
    #         total_preto += ref.amount

    # tot_gen = total_cape + total_jozzy + total_preto + total_vaal
    # return HttpResponse('Total des ravitaillements sur les branches RSA entre le 01 Septembre et 31 Octobre 2017<br/>Total sur Pretoria s.a: R ' + to_readable(total_preto) + '<br/>Total sur Vaal s.a: R ' + to_readable(total_vaal) + '<br/>Total sur Johhannesburg s.a: R ' + to_readable(total_jozzy) + '<br/>Total sur Capetown s.a: R ' + to_readable(total_cape) + '<br/>Total General: R ' + to_readable(tot_gen))
    return HttpResponse("Done. {0} New fidelity built".format(number_of_new_fidelity_created))




def rate_scrapper(request):
    branches = Branch.objects.all()
    return render(request, 'app/dashboard.html', {'branches':branches})

def daily_money(request):
    currentday_transactions = Transaction.objects.filter(time__lte= parser.parse("Dec 20 2017 11:59PM")).filter(time__gte= parser.parse("Dec 20 2017 00:00AM")).filter(author_id = 2)|Transaction.objects.filter(time__lte= parser.parse("Dec 20 2017 11:59PM")).filter(time__gte= parser.parse("Dec 20 2017 00:00AM")).filter(author_id = 3)
    total_day = 0.0

    for transaction in currentday_transactions:
        if transaction.type == 'Transfer':
            transfer = Transfer.objects.get(transactionDetails_id = transaction.id)
            computation = Transfer_Computation.objects.get(id = transfer.computationDetails_id)
            total_day += computation.amount

    return HttpResponse("{0}".format(to_readable(total_day)))


def client_killer(request):
    total_vdb = 0.0
    total_jhb = 0.0
    total_pta = 0.0
    total_lbv = 0.0
    total_pog = 0.0

    for transaction in Transaction.objects.filter(type='Transfer').filter(time__lte=parser.parse("Dec 12 2017 11:59PM")):
        transfer = Transfer.objects.get(transactionDetails_id=transaction.id)
        computation = Transfer_Computation.objects.get(
            id=transfer.computationDetails_id)

        if computation.branch_to == 'port gentil' or computation.branch_to == 'Libreville centrale':
            if computation.branch_to == 'port gentil':
                total_pog += computation.total_if_rate
            else:
                total_lbv += computation.total_if_rate

            if transaction.author_id == 4:
                total_pta += computation.amount

            if transaction.author_id == 5:
                total_jhb += computation.amount

            if transaction.author_id == 6:
                total_vdb += computation.amount

    return HttpResponse('Pretoria: ' + to_readable(total_pta) + '<br/>Johannesburg: ' + to_readable(total_jhb) + '<br/>Vaal: ' + to_readable(total_vdb) + '<br/>Port Gentil: ' + to_readable(total_pog) + '<br/>Libreville: ' + to_readable(total_lbv))


def get_branches(request):
    if request.session.get('current_branch_id') == 8:
        return Branch.objects.all()
    else:
        return Branch.objects.exclude(id=1).exclude(id=8)


def quotation_pages(request, service_name):
    form = Transfer_ComputationForm()
    branches = get_branches(request)
    rates = Rates.objects.all()
    currencies = Currency.objects.all()

    for branch in branches:
        try:
            branch.currency = Currency.objects.get(name=branch.currency).symbol
        except:
            pass

    request.session['service'] = service_name
    return render(request, 'app/public/quotation.html', {'form': form, 'branches': branches, 'rates': rates, 'currencies':currencies})


def transactions_from_origin(request):
    transfers_list = []
    list_of_transfer_transactions = Transaction.objects.filter(type='Transfer')

    for branch in get_branches(request):

        trans_in = []
        total_in = 0.0
        trans_out = []
        total_out = 0.0
        trans_in.append('{0} - Recus'.format(branch.name))

        trans_out.append('{0} - Envoyes'.format(branch.name))
        for transaction in list_of_transfer_transactions:
            transfer = Transfer.objects.get(
                transactionDetails_id=transaction.id)
            computation = Transfer_Computation.objects.get(
                id=transfer.computationDetails_id)

            if transaction.author_id == branch.id:
                trans_out.append(computation.amount)
                total_out += computation.amount
            else:
                trans_out.append(0)

            if computation.branch_to == branch.name:
                trans_in.append(computation.total_if_rate)
                total_in += computation.total_if_rate
            else:
                trans_in.append(0)

        trans_in.append('Total: {0} - Recus'.format(total_in))
        trans_out.append('Total {0} - Envoyes'.format(total_out))
        transfers_list_branch = zip(trans_out, trans_in)
        transfers_list.append(transfers_list_branch)

    return render(request, 'app/origin.html', {'transfers_list': transfers_list, 'size': len(transfers_list)})


def rebalancer(request):
    for client in Client.objects.all():
        client.worth = 0.0
        client.save()

    # Date where me and Arsene resetted all the branches balance to 0
    date_apocalypse = parser.parse("Oct 25 2017 10:59AM")
    # Now we treat the transfers first.
    # the date should change after the gift is given
    for transaction in Transaction.objects.filter(type='Transfer').filter(time__gte=date_apocalypse):

        # get the transfer, get its computation and substract the amount to the balance
        transfer = Transfer.objects.get(transactionDetails_id=transaction.id)
        computation = Transfer_Computation.objects.get(
            id=transfer.computationDetails_id)
        receiver = Client.objects.get(id=transfer.receiver_id)
        receiver.worth += computation.total_if_rate * 0.0016
        receiver.save()

        sender = Client.objects.get(id=transfer.sender_id)
        sender.worth += computation.total_to_pay *  1.0016
        sender.save()

    all_branch = Branch.objects.all()
    for branch_to_treat in all_branch:
        branch_to_treat.current_balance = 0.0
        # Now we treat the transfers first.
        for transaction in Transaction.objects.filter(type='Transfer').filter(time__gte=date_apocalypse):
                # get the transfer, get its computation and substract the amount to the balance
            transfer = Transfer.objects.get(
                transactionDetails_id=transaction.id)
            computation = Transfer_Computation.objects.get(
                id=transfer.computationDetails_id)

            # We treat the two cases:
            if transaction.author_id == branch_to_treat.id:
                branch_to_treat.current_balance += computation.total_to_pay
                print('{0} + {1}'.format(branch_to_treat.current_balance,
                                         computation.total_to_pay))

            if computation.branch_to == branch_to_treat.name and transfer.isCollected == True:
                branch_to_treat.current_balance -= computation.total_if_rate
                print('{0} - {1}'.format(branch_to_treat.current_balance,
                                         computation.total_if_rate))

        # Now we treat the refuelings.
        for transaction in Transaction.objects.filter(type='Ravitaillement').filter(time__gte=date_apocalypse):
            refueling = Refueling.objects.get(
                transactionDetails_id=transaction.id)
            if branch_to_treat.id == refueling.branch_to_id:
                branch_to_treat.current_balance += refueling.amount
                print(
                    '{0} + {1}'.format(branch_to_treat.current_balance, refueling.amount))

        # Now we treat the expenses
        for transaction in Transaction.objects.filter(type='Expense').filter(time__gte=date_apocalypse):
            expense = Expense.objects.get(transactionDetails_id=transaction.id)
            if branch_to_treat.id == transaction.author_id:
                branch_to_treat.current_balance -= expense.amount
                print(
                    '{0} - {1}'.format(branch_to_treat.current_balance, expense.amount))

        branch_to_treat.save()
        branch_to_treat.current_balance = to_readable(
            branch_to_treat.current_balance)
    return render(request, 'app/rebalancer.html', {'branches': all_branch})


def update_balance(request, branch_id):
    # Date where me and Arsene resetted all the branches balance to 0
    date_apocalypse = parser.parse("Oct 25 2017 10:59AM")
    branch_to_treat = Branch.objects.get(id=branch_id)

    branch_to_treat.current_balance = 0.0

    # Now we treat the transfers first.
    for transaction in Transaction.objects.filter(type='Transfer').filter(time__gte=date_apocalypse):
        # get the transfer, get its computation and substract the amount to the balance
        transfer = Transfer.objects.get(transactionDetails_id=transaction.id)
        computation = Transfer_Computation.objects.get(
            id=transfer.computationDetails_id)

        # We treat the two cases:
        if transaction.author_id == branch_to_treat.id:
            branch_to_treat.current_balance += computation.total_to_pay
            print('{0} + {1}'.format(branch_to_treat.current_balance,
                                     computation.total_to_pay))

        if computation.branch_to == branch_to_treat.name and transfer.isCollected == True:
            branch_to_treat.current_balance -= computation.total_if_rate
            print('{0} - {1}'.format(branch_to_treat.current_balance,
                                     computation.total_if_rate))

    # Now we treat the refuelings.
    for transaction in Transaction.objects.filter(type='Ravitaillement').filter(time__gte=date_apocalypse):
        refueling = Refueling.objects.get(transactionDetails_id=transaction.id)
        if branch_to_treat.id == refueling.branch_to_id:
            branch_to_treat.current_balance += refueling.amount
            print('{0} + {1}'.format(branch_to_treat.current_balance, refueling.amount))

    # Now we treat the expenses
    for transaction in Transaction.objects.filter(type='Expense').filter(time__gte=date_apocalypse):
        expense = Expense.objects.get(transactionDetails_id=transaction.id)
        if branch_to_treat.id == transaction.author_id:
            branch_to_treat.current_balance -= expense.amount
            print('{0} - {1}'.format(branch_to_treat.current_balance, expense.amount))

    branch_to_treat.save()
    branch_to_treat.current_balance = to_readable(
        branch_to_treat.current_balance)


# =============== Task: Check if a branch is well connected and if has the privilege to be logged in =====================
def security_checks(request, current_branch_id):
    if current_branch_id == "":
        return redirect(logout)
    else:
        branch = Branch.objects.get(
            id=request.session.get('current_branch_id'))
        if branch.isLoggedIn == False:
            return redirect(logout)


# =============== Float to formatted string =====================
def insert_space(string, index):
    '''This method add insert a space character at a specified index
        and returns the new string'''

    return string[:index] + ',' + string[index:]


def to_readable(f):
    ''' This method simply returns a float as an easily readable string
        with space every three carateres'''
    isNegative = False

    f = str(round(f, 2))

    if '-' in f:
        isNegative = True
        f = f[1:]

    dot_index = f.index('.')

    range_stop = int(dot_index / 3)

    if dot_index % 3 == 0:
        range_stop -= 1

    for i in range(0, range_stop):
        dot_index -= 3
        f = insert_space(f, dot_index)

    # take the negative sign  back
    if isNegative:
        f = '-' + f

    return str(f)

# =============== Balance Tracker =====================


def balance_tracker(request, branch_id):
    branch_to_treat = Branch.objects.get(id=branch_id)
    date_begin = None
    date_end = None
    number_of_transactions = 0
    total_debit = 0.0
    total_credit = 0.0
    total_comission_debit = 0.0
    total_comission_credit = 0.0

    # Now we collect all the transactions to be worked on
    list_of_transactions_on_selected_period = []
    # date, type or code if transfer, amount subtracted, amount added, branch of the balance minus those two amount
    # transaction.time,transfer.reference + ':PayeeOrNot | ' + receiver.surname + ' ' + receiver.names, 0.0, to_readable(computation.total_to_pay), to_readable(opening_balance
    # str_date
    # str_details
    # str_debit
    # str_credit
    # str_balance

    # all_branches = Branch.objects.exclude(id=1)

    # for branch in all_branches:
    #     branch.current_balance = 0.0
    branch_to_treat.current_balance = 0.0

    if request.method == 'POST':
        picked_range = request.POST.get('range-picker', '')
        begin, end = picked_range.split('-')
        # Date where me and Arsene resetted all the branches balance to 0
        date_apocalypse = parser.parse("Oct 25 2017 10:59AM")
        date_begin = parser.parse(begin)
        date_end = parser.parse(end)

        # Now we treat the transfers first.
        for transaction in Transaction.objects.filter(type='Transfer').filter(time__lte=date_begin).filter(time__gte=date_apocalypse):
            # get the transfer, get its computation and substract the amount to the balance

            transfer = Transfer.objects.get(transactionDetails_id=transaction.id)
            computation = Transfer_Computation.objects.get(
                id=transfer.computationDetails_id)

            # We treat the two cases:
            if transaction.author_id == branch_to_treat.id:
                branch_to_treat.current_balance += computation.total_to_pay

            if computation.branch_to == branch_to_treat.name:
                if transfer.isCollected == True:
                    branch_to_treat.current_balance -= computation.total_if_rate

        # Now we treat the refuelings.
        for transaction in Transaction.objects.filter(type='Ravitaillement').filter(time__lte=date_begin).filter(time__gte=date_apocalypse):
            refueling = Refueling.objects.get(
                transactionDetails_id=transaction.id)
            if branch_to_treat.id == refueling.branch_to_id:
                branch_to_treat.current_balance += refueling.amount

        # Now we treat the expenses
        for transaction in Transaction.objects.filter(type='Expense').filter(time__lte=date_begin).filter(time__gte=date_apocalypse):
            expense = Expense.objects.get(transactionDetails_id=transaction.id)
            if branch_to_treat.id == transaction.author_id:
                branch_to_treat.current_balance -= expense.amount

        #branch_to_treat = next((x for x in all_branches if x.id == branch_to_treat.id), None)
        opening_balance = branch_to_treat.current_balance

        for transaction in Transaction.objects.filter(time__lte=date_end).filter(time__gte=date_begin).filter(time__gte=date_apocalypse):
            comission_credit = 0.0
            comission_debit = 0.0
        #################################################################################################################################################
            # if it's a transfer
            if transaction.type == 'Transfer':

                id_to_catch = transaction.id
                transfer = Transfer.objects.get(
                    transactionDetails_id=transaction.id)
                receiver = Client.objects.get(id=transfer.receiver_id)
                computation = Transfer_Computation.objects.get(
                    id=transfer.computationDetails_id)
                if transaction.author_id == branch_to_treat.id:
                    opening_balance += computation.total_to_pay
                    total_credit += computation.total_to_pay
                    comission_credit = (computation.total_to_pay * 0.01) / 1.065
                    total_comission_credit += comission_credit

                    # add to list_of_transactions_on_selected_period
                    if(transfer.isCollected == True):
                        list_of_transactions_on_selected_period.append((transfer.id, transaction.time, transfer.reference + ':Payee | ' + receiver.surname + ' ' + receiver.names, 0.0, to_readable(
                            comission_debit), to_readable(computation.total_to_pay), to_readable(comission_credit), to_readable(opening_balance)))
                    else:
                        list_of_transactions_on_selected_period.append((transfer.id, transaction.time, transfer.reference + ' ' + receiver.surname + ' ' + receiver.names, 0.0, to_readable(
                            comission_debit), to_readable(computation.total_to_pay), to_readable(comission_credit), to_readable(opening_balance)))

                if branch_to_treat.name == computation.branch_to:
                    opening_balance -= computation.total_if_rate
                    total_debit += computation.total_if_rate
                    comission_debit = computation.total_if_rate * 0.01
                    total_comission_debit += comission_debit

                    if transfer.isCollected == True:
                        # add to list_of_transactions_on_selected_period
                        list_of_transactions_on_selected_period.append((transfer.id, transaction.time, transfer.reference + ':Payee | ' + receiver.surname + ' ' + receiver.names, to_readable(
                            computation.total_if_rate), to_readable(comission_debit), 0.0, to_readable(comission_credit), to_readable(opening_balance)))
                    else:
                        list_of_transactions_on_selected_period.append((transfer.id, transaction.time, transfer.reference + ': ' + receiver.surname + ' ' + receiver.names, to_readable(
                            computation.total_if_rate), to_readable(comission_debit), 0.0, to_readable(comission_credit), to_readable(opening_balance)))

            #if it's a refueling
            if transaction.type == 'Ravitaillement':
                if transaction.author_id == branch_to_treat.id:
                    refueling = Refueling.objects.get(
                        transactionDetails_id=transaction.id)
                    opening_balance += refueling.amount
                    total_credit += refueling.amount
                    _str = "{0} + {1}".format(total_credit, refueling.amount)
                    #assert False

                    # add to list_of_transactions_on_selected_period
                    list_of_transactions_on_selected_period.append(('#', transaction.time, 'Ravitaillement', 0.0, to_readable(
                        comission_debit), to_readable(refueling.amount), to_readable(comission_credit), to_readable(opening_balance)))

            # if it's an expense
            if transaction.type == 'Expense':
                expense = Expense.objects.get(
                    transactionDetails_id=transaction.id)
                if branch_to_treat.id == transaction.author_id:
                    opening_balance -= expense.amount
                    total_debit += expense.amount

                    list_of_transactions_on_selected_period.append(('#', transaction.time, expense.type, to_readable(
                        expense.amount), to_readable(comission_debit), 0.0, to_readable(comission_credit), to_readable(opening_balance)))
        branch_to_treat.current_balance = to_readable(
            branch_to_treat.current_balance)
        number_of_transactions = len(list_of_transactions_on_selected_period)
    return render(request, 'app/Branch/balance_tracker.html', {'total_comission_debit': to_readable(total_comission_debit), 'total_comission_credit': to_readable(total_comission_credit), 'total_credit': to_readable(total_credit), 'total_debit': to_readable(total_debit), 'branch_to_treat': branch_to_treat, 'number_of_transactions': number_of_transactions, 'list_of_transactions_on_selected_period': list_of_transactions_on_selected_period, 'date_begin': date_begin, 'date_end': date_end})


# =============== Customer/Client Space =====================
    # for the admin branch, we should have the posssiblity to see all the clients while for
    # a specific other branch; only clients registered through that branch should be available

def edit_client_details(request, client_id):
    client_to_edit = Client.objects.get(id=client_id)

    return render(request, 'app/Client/edit.html', {'client': client_to_edit})


def client_details(request, id):
    client_to_treat = Client.objects.get(pk=id)
    client_branch = Branch.objects.get(id=client_to_treat.branch_id)
    # received transfers list computation
    received_transfer_list = Transfer.objects.filter(
        receiver_id=client_to_treat.id)

    # sent transfers list computation
    sent_transfer_list = Transfer.objects.filter(sender_id=client_to_treat.id)

    # compute received transfer output list
    output_received = []
    for tr in received_transfer_list:
        # get the transaction
        transaction = Transaction.objects.get(id=tr.transactionDetails_id)
        # compute sender branch
        sender_branch = Branch.objects.get(id=transaction.author_id)
        sender = Client.objects.get(id=tr.sender_id)
        # compute computation details
        comp = Transfer_Computation.objects.get(id=tr.computationDetails_id)

        output_received.append((transaction.time, tr.reference, sender_branch.name, to_readable(
            comp.amount), sender.surname + " " + sender.names, tr.id))

    # compute sent output list
    output_sent = []
    for tr in sent_transfer_list:
        # get the transaction
        try:
            transaction = Transaction.objects.get(id=tr.transactionDetails_id)
            # compute sender branch
            sender_branch = Branch.objects.get(id=transaction.author_id)
            receiver = Client.objects.get(id=tr.receiver_id)
            # compute computation details
            comp = Transfer_Computation.objects.get(id=tr.computationDetails_id)

            output_sent.append((transaction.time, tr.reference, sender_branch.name, comp.amount, receiver.surname + " " + receiver.names, tr.id))
        except:
            pass

    client_to_treat.worth = to_readable(client_to_treat.worth)
    return render(request, 'app/Branch/client_details.html', {'client_branch':client_branch ,'client': client_to_treat, 'output_received': output_received, 'output_sent': output_sent})


def branch_list_of_client(request):
    all_branches = get_branches(request)
    branch_to_treat = None

    if request.method == 'POST':
        branch_to_treat = request.POST.get('branch', '')

    else:
        branch_to_treat = request.session.get('current_branch_name')

    branch_clients = Client.objects.filter(
        branch_id=Branch.objects.get(name=branch_to_treat).id).order_by('surname')
    count = len(list(branch_clients))
    count_total = Client.objects.all().count()
    return render(request, 'app/Branch/branch_list_of_client.html', {'count_total': count_total, 'all_branches': all_branches, 'branch_clients': branch_clients, 'branch_to_treat': branch_to_treat, 'count': count})


def new_client(request):
    ################################## Security checks ##################################
    if 'current_branch_id' not in request.session:
        return redirect(logout)
    else:
        branch = Branch.objects.get(
            id=request.session.get('current_branch_id'))
        if branch.isLoggedIn == False:
            return redirect(logout)

    #######################################################################################

    # branches list to forward to the template
    branches = get_branches(request)
    # True = Registration made successfully, False = An error occured
    registration_status = None
    new_client = None

    if request.method == 'POST':
        # form validation

        if request.POST.get('surname', '') == '' or request.POST.get('names', '') == '' or request.POST.get('contactNumber', '') == '' or request.POST.get('Branch', '') == '':
            registration_status = False
        else:
            request.session['show_register_form_validation_error'] = False
            try:
                new_client_branch = Branch.objects.get(
                    name=request.POST.get('Branch', ''))
                new_client = Client(surname=request.POST.get('surname', ''), names=request.POST.get('names', ''), email=request.POST.get('Email', ''), contactNumber=request.POST.get(
                    'contactNumber', '').replace(' ', ''), branch=new_client_branch, bank_name=request.POST.get('bank_name', ''), bank_account_number=request.POST.get('bank_account_number', ''))

                new_client.save()
                registration_status = True

                if 'computation_id' in request.session:
                    if request.session.get('sender_id') == None and request.session.get('current_client_taking') == 'Expediteur':
                        request.session['sender_id'] = new_client.id
                        request.session['isSenderSaved'] = True
                        request.session['current_client_taking'] = 'Destinataire'

                        return redirect(get_receiver)

                    if request.session.get('sender_id') != None and request.session.get('current_client_taking') == 'Destinataire':
                        request.session['receiver_id'] = new_client.id
                        # Save the transfer transaction/Send the money
                        return redirect(save_Transfer)

            except IntegrityError as e:
                if 'unique constraint' in str(e):
                    registration_status = 'exists'
            except:
                registration_status = False
    return render(request, 'app/Branch/Register.html', {'registration_status': registration_status, 'new_client': new_client, 'list_of_branches': branches})


def check_client(request, client_id):
    client = Client.objects.get(id=client_id)
    client_branch = Branch.objects.get(id=client.branch_id)
    other_branch = get_branches(request)

    if request.method == 'POST':
        client.surname = request.POST.get('surname', '')
        client.names = request.POST.get('names', '')
        client.email = request.POST.get('Email', '')
        client.contactNumber = request.POST.get('contactNumber', '')
        client.branch_id = Branch.objects.get(
            name=request.POST.get('Branch', ''))
        client.bank_name = request.POST.get('bank_name', '')
        client.bank_account_number = request.POST.get(
            'bank_account_number', '')
        client.birthdate = parser.parse(request.POST.get('birthdate', ''))
        client.save()

        if request.session.get('current_client_taking') == 'Destinataire':
            return redirect(get_receiver)
        elif request.session.get('save_transfer') == True:
            request.session['save_transfer'] = False
            return redirect(save_Transfer)
        elif request.session.get('current_transaction') == None:
            redirect_url = reverse('client_details', kwargs={"id": client.id})
            return HttpResponseRedirect(redirect_url)
    else:
        return render(request, 'app/Client/edit.html', {'client': client, 'client_branch': client_branch, 'other_branch': other_branch})


# ================== Administratioin ========================
def cancel_refueling(request):
    request.session['ready_to_refuel'] = False
    return redirect(admin_branch)


def refuel_branch(request, id):
    request.session['display_computation'] = False

    branch = Branch.objects.get(pk=id)
    try:
        currency = Currency.objects.get(name=branch.currency)
    except:
        currency = None

    if request.method == 'POST':
        if request.POST.get('amount', '') != '':
            request.session['display_computation'] = True
            request.session['display_error'] = False
            request.session['refuel_amount'] = float(
                request.POST.get('amount', ''))
            request.session['next_balance'] = branch.current_balance + float(request.POST.get('amount', ''))
            request.session['ready_to_refuel'] = True

        else:
            request.session['display_error'] = True
    else:
        request.session['display_error'] = False
        if request.session.get('ready_to_refuel') == True:
            request.session['ready_to_refuel'] = False
            branch.current_balance = float(request.session.get('next_balance'))
            branch.save()

            # Now we need to add it to the receiving brach as a transfer made(Money received from the client)
            # Create the transaction object
            # Since arsene does not have money and does not do the transactions
            new_transaction = Transaction(type='Ravitaillement', author=branch)
            new_transaction.save()

            # The refuelinig object
            new_refuel = Refueling(amount=float(request.session.get(
                'refuel_amount')), transactionDetails=new_transaction, branch_to=branch)
            new_refuel.save()

            return redirect(admin_branch)

    return render(request, 'app/settings/refueling.html', {'currency': currency, 'branch': branch, })


def add_currency(request):
    if request.method == 'POST':
        form = CurrencyForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(dashboard)
        else:
            return HttpResponse('0')
    else:
        form = CurrencyForm()
        return render(request, 'app/settings/add_currency.html', {'form': form})


def add_branch(request):
    if request.method == 'POST':
        form = BranchForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect(admin_branch)
    else:
        form = BranchForm()
        return render(request, 'app/settings/new_branch.html', {'form': form})


def edit_branch(request, id):
    branch_to_edit = Branch.objects.get(pk=id)
    list_of_currencies = Currency.objects.all()
    try:
        branch_to_edit_currency = Currency.objects.get(
            name=branch_to_edit.currency)
    except:
        branch_to_edit_currency = None

    if request.method == 'POST':
        branch_to_edit.name = request.POST.get('name', '')
        branch_to_edit.passPhrase = request.POST.get('pass', '')
        branch_to_edit.currency = request.POST.get('currency', '')
        branch_to_edit.save()

        return redirect(admin_branch)

    else:

        return render(request, 'app/settings/edit_branch.html', {'branch_to_edit_currency': branch_to_edit_currency, 'list_of_currencies': list_of_currencies, 'branch_to_edit': branch_to_edit})


def delete_branch(request, id):
    try:
        branch = Branch.objects.get(pk=id)
        branch.delete()
    except:
        pass
    return HttpResponseRedirect(reverse('admin_branch'))


def costs_delete(request, id):
    cost = Cost_Percentage.objects.get(id=id)
    cost.delete()
    return redirect(costs)


def costs(request):
    form = None
    all_costs = Cost_Percentage.objects.all()
    if request.method == 'POST':
        form = Cost_PercentageForm(request.POST)
        if form.is_valid():
            try:
                cost = Cost_Percentage.objects.get(
                    item=form.cleaned_data['item'])
                cost.percentage = form.cleaned_data['percentage']
                cost.save()
            except Cost_Percentage.DoesNotExist:
                form.save()
            return redirect(costs)
    else:
        form = Cost_PercentageForm()
        return render(request, 'app/settings/edit_costs.html', {'all_costs': all_costs, 'form': form})


def rates(request):
    all_rates = Rates.objects.all()
    if request.method == 'POST':
        form = RatesForm(request.POST)
        if form.is_valid():
            try:
                rate = Rates.objects.get(
                    currency_from=form.cleaned_data['currency_from'], currency_to=form.cleaned_data['currency_to'])
                rate.value = form.cleaned_data['value']
                rate.save()
            except Rates.DoesNotExist:
                form.save()
        return redirect(rates)
    else:
        form = RatesForm()
        return render(request, 'app/settings/edit_rates.html', {'rates': all_rates, 'form': form})


def admin_branch(request):
    # get all the branches
    all_the_branches = get_branches(request)
    for branch in all_the_branches:
        branch.current_balance = to_readable(branch.current_balance)

    return render(request, 'app/settings/admin_branch.html', {'all_the_branches': all_the_branches})


def freez_branch(request, branch_id):
    branch_to_freez = Branch.objects.get(id=branch_id)
    branch_to_freez.isFrozen = True
    branch_to_freez.isLoggedIn = False
    branch_to_freez.save()
    return redirect(admin_branch)


def unfreez_branch(request, branch_id):
    branch_to_freez = Branch.objects.get(id=branch_id)
    branch_to_freez.isFrozen = False
    branch_to_freez.isLoggedIn = True
    branch_to_freez.save()
    return redirect(admin_branch)


def self_unfreez(request, branch_id):
    branch_to_freez = Branch.objects.get(id=branch_id)
    branch_to_freez.isFrozen = False
    branch_to_freez.save()
    return redirect(dashboard)

# ===============================================================
# ================== Money transactions =========================
# ===============================================================

# ########################## Save an expense made by the branch ##########################


def transfer_branch_expenses(request):
    # Display the form
    request.session['error'] = ''
    if request.method == 'POST':
        if request.POST.get('type', '') != '' and request.POST.get('amount', '') != '':
            try:
                # Create the transaction object
                tr_author = Branch.objects.get(
                    name=request.session.get('current_branch_name'))
                new_transaction = Transaction(type='Expense', author=tr_author)
                new_transaction.save()

                # Create the expense object
                expense_amount = float(request.POST.get('amount', ''))
                new_expense = Expense(type=request.POST.get(
                    'type', ''), amount=expense_amount, transactionDetails=new_transaction)
                new_expense.save()

                # affect the current branch's balance
                tr_author.current_balance -= new_expense.amount
                tr_author.save()

                update_branch_session_vriables(
                    request, request.session.get('current_branch_id'))

            except:
                request.session['error'] = 'Veuillez entrer un montant valide'

        else:
            request.session['error'] = 'Veuillez entrer selctioner un type et entrer un montant valide'

    return render(request, 'app/Transfer/save_branch_expenses.html', {})


# ########################## Send Money ##########################
# Save the transfer(Send Money) and display the summary
def save_Transfer(request):
    sender = request.session.get('sender_id')
    receiver = request.session.get('receiver_id')
    # Get sender and receiver
    tr_sender = Client.objects.get(id=request.session.get('sender_id'))
    tr_receiver = Client.objects.get(id=request.session.get('receiver_id'))

    # Get the computation
    computation = Transfer_Computation.objects.get(
        id=request.session.get('computation_id'))

    # Create the transaction object
    tr_author = Branch.objects.get(
        name=request.session.get('current_branch_name'))
    new_transaction = Transaction(type='Transfer', author=tr_author)
    new_transaction.save()

    # build the reference number of the transaction
    referenceNumber = "{}-{}".format(
        tr_sender.surname[0:2].upper(), random.randint(10000, 99999))

    # And then without further due, we build the transfert object and save it
    new_transfer = Transfer(transactionDetails=new_transaction, computationDetails=computation,
                            receiver=tr_receiver, sender=tr_sender, reference=referenceNumber)
    new_transfer.save()

    # affect the current branch's balance |We add the total inluding rates to the sending branch but substract the equivalent without the rates to the receiving branch(When we mark it as a paid transfer)
    tr_author = Branch.objects.get(id=request.session.get('current_branch_id'))
    tr_author.current_balance += computation.total_to_pay
    tr_author.save()

    # receiving_branch = Branch.objects.get(name = computation.branch_to)
    # receiving_branch.current_balance -= computation.total_if_rate
    # receiving_branch.save()

    update_branch_session_vriables(
        request, request.session.get('current_branch_id'))

    # Clear all the used sessions in this method
    del request.session['computation_id']
    del request.session['sender_id']
    del request.session['receiver_id']
    # Clean it to avoid jumping steps and causing bugs
    request.session['current_transaction'] = None
    request.session.modified = True  # tell that session changed state, shoul refresh

    redirect_url = reverse('transaction_details', kwargs={
                           "tr_id": new_transfer.id})
    return HttpResponseRedirect(redirect_url)


# Verify if the number entered belong to a client. If yes, the he exists
def isClient(request):
    is_Client = None

    if request.method == 'GET':
        request.session['show_results'] = False
        return render(request, 'app/Transfer/hi_search.html', {'isClient': is_Client})

    if request.method == 'POST':
        clients = []
        for client in Client.objects.all():
            keyword = request.POST.get('keyword', '')
            if str.isdigit(keyword.replace(' ', '')):
                request.session['keyword_value_type'] = 'Numéro de téléphone'
                keyword = keyword.replace(' ', '')
                if keyword in client.contactNumber.replace('+', '00') or keyword == client.contactNumber.replace('+', '00'):
                    clients.append(client)
            else:
                request.session['keyword_value_type'] = 'nom ou/et prénom'
                keyword = keyword.lower()
                names_string = (client.surname + ' ' + client.names).lower()

                if keyword in names_string or keyword == names_string:
                    clients.append(client)

        number_of_hit = len(clients)
        request.session['show_results'] = True
        return render(request, 'app/Transfer/hi_search.html', {'keyword': keyword, 'number_of_hit': number_of_hit, 'clients': clients})


def process_transaction_actor(request, client_id):
    client_found = Client.objects.get(id=client_id)
    if request.session.get('current_client_taking') == 'Expediteur':
        request.session['sender_id'] = client_found.id
        # Changed so that we carry on to get the receiver
        request.session['current_client_taking'] = 'Destinataire'
        redirect_url = reverse('check_client', kwargs={
                               "client_id": client_found.id})
        return HttpResponseRedirect(redirect_url)

    if request.session.get('current_client_taking') == 'Destinataire':
        request.session['receiver_id'] = client_found.id
        request.session['current_client_taking'] = None
        request.session['save_transfer'] = True
        redirect_url = reverse('check_client', kwargs={
                               "client_id": client_found.id})
        return HttpResponseRedirect(redirect_url)


# Get the client to receive the money
def get_receiver(request):
    request.session['current_client_taking'] = 'Destinataire'
    request.session['receiver_id'] = None
    return redirect(isClient)


# Get the client sending the money
def get_sender(request):
    request.session['sender_id'] = None
    request.session['current_client_taking'] = 'Expediteur'
    return redirect(isClient)


# Computation done around funds to be sent before getting the sender
# and the receiver.
def send_money(request):
    ################################## Security checks ##################################
    if 'current_branch_id' not in request.session:
        return redirect(logout)
    else:
        branch = Branch.objects.get(
            id=request.session.get('current_branch_id'))
        if branch.isLoggedIn == False:
            return redirect(logout)

    #######################################################################################

    # Determine the type of transaction being in process
    request.session['current_transaction'] = 'Transfer'

    # This variable serves to tell if we should whether or not display the computation HTML
    request.session['display_computation'] = False
    current_branch = Branch.objects.get(
        name=request.session.get('current_branch_name'))

    curent_branch_currency = Currency.objects.get(name=current_branch.currency)

    # variables
    list_of_applied_costs = []
    total_to_pay = 0.0
    transaction_amount = 0.0
    transfer_currency = None
    rate = Rates('', '', 1.0)
    total_if_rate = 0.0

    form = None
    if request.method == 'POST':
        try:
            if 'computation_id' in request.session:
                tr_computation = Transfer_Computation.objects.get(
                    id=request.session.get('computation_id'))
            else:
                tr_computation = Transfer_Computation()
                #tr_computation.id = request.session.get('computation_id')
        except:
            tr_computation = Transfer_Computation()

        request.session['display_computation'] = True
        form = Transfer_ComputationForm(request.POST)
        if form.is_valid():
            # Now we compute everything by fetching the values from the from and send waht need to be sent
            # Computation session variables
            transaction_amount = float(form.cleaned_data['amount'])

            branch_from_form = None
            if request.session.get('current_branch_name') == 'nat_development':
                branch_from_form = Branch.objects.get(name='nat_development')
            else:
                branch_from_form = Branch.objects.get(
                    name=form.cleaned_data['branch_to'])

            transfer_currency = Currency.objects.get(
                name=branch_from_form.currency)

            total_costs_percentage = 0.0
            for cost in form.cleaned_data['applied_costs']:
                list_of_applied_costs.append(
                    (cost.item, (cost.percentage / 100) * transaction_amount))
                total_costs_percentage = total_costs_percentage + \
                    (cost.percentage / 100)
            total_to_pay = transaction_amount + \
                (transaction_amount * total_costs_percentage)

            # Compute the rate.
            if transfer_currency.name != current_branch.currency:
                if transfer_currency.name == 'Rand' and current_branch.currency == 'Franc CFA':
                    rate.value = Rates.objects.get(
                        currency_from='EUR', currency_to='ZAR').value / Rates.objects.get(currency_from='EUR', currency_to='XAF').value

                elif transfer_currency.name == 'Franc CFA' and current_branch.currency == 'Rand':
                    rate.value = Rates.objects.get(
                        currency_from='EUR', currency_to='XAF').value / Rates.objects.get(currency_from='ZAR', currency_to='EUR').value

                else:
                    rate = Rates.objects.get(currency_from=transfer_currency.symbol, currency_to=Currency.objects.get(
                        name=current_branch.currency).symbol)

            else:
                rate.value = 1.0

            total_if_rate = transaction_amount * rate.value

            # commented cause I moved it upper
            # now we contact the db to save the computation and we can still edit due to the fact that we are saving its ID
            # try:
            #     if computation_id in request.session:
            #         tr_computation = Transfer_Computation.objects.get(id=request.session.get('computation_id'))
            #         #tr_computation.id = request.session.get('computation_id')
            # except:
            #     tr_computation = Transfer_Computation()

            # apply the id if saved in the session already
            tr_computation.amount = form.cleaned_data['amount']
            tr_computation.currency_from = branch_from_form.currency
            tr_computation.branch_to = branch_from_form.name
            tr_computation.total_if_rate = total_if_rate
            tr_computation.total_to_pay = total_to_pay
            tr_computation.total_costs_percentage = total_costs_percentage

            tr_computation.list_applied_costs = ''  # clean the existing value
            for cost, value in list_of_applied_costs:
                tr_computation.list_applied_costs += '({}:{})'.format(
                    cost, value)

            tr_computation.save()
            request.session['computation_id'] = tr_computation.id

    else:
        form = Transfer_ComputationForm()

    return render(request, 'app/Transfer/compute.html', {'form': form, 'list_of_applied_costs': list_of_applied_costs, 'transaction_amount': transaction_amount, 'total_to_pay': total_to_pay, 'transfer_currency': transfer_currency, 'current_branch': current_branch, 'rate': rate, 'total_if_rate': total_if_rate})


# This method allows to cancel a transfer transaction at anytime by
# deleting relevant session data and object
def cancel_transaction(request):
    if 'computation_id' in request.session:
        Transfer_Computation.objects.get(
            id=request.session.get('computation_id')).delete()
        del request.session['computation_id']
        # Clear all the used sessions in this method

    if 'sender_id' in request.session:
        del request.session['sender_id']

    if 'current_transaction' in request.session:
        del request.session['current_transaction']

    if 'current_client_taking' in request.session:
        del request.session['current_client_taking']

    if 'save_transfer' in request.session:
        del request.session['save_transfer']

    if 'receiver_id' in request.session:
        del request.session['receiver_id']

    # Clean it to avoid jumping steps and causing bugs
    request.session['current_transaction'] = None

    request.session.modified = True  # tell that session changed state, shoul refresh
    return redirect(dashboard)

# ########################## Transaction/Transfer Details ##########################

# Cancel/Delete Transfer


def delete_transfer(request, tr_id):
    transfer_object = Transfer.objects.get(id=tr_id)
    transaction_object = Transaction.objects.get(
        id=transfer_object.transactionDetails_id)
    computation = Transfer_Computation.objects.get(
        id=transfer_object.computationDetails_id)

    sending_branch = Branch.objects.get(id=transaction_object.author_id)
    receiving_branch = Branch.objects.get(name=computation.branch_to)

    # update branch balances
    sending_branch.current_balance -= computation.total_to_pay
    sending_branch.save()

    # the following line is only important iff We are testing with arsene
    update_branch_session_vriables(
        request, request.session.get('current_branch_id'))

    # Delete those objects in order
    transfer_object.delete()
    computation.delete()
    transaction_object.delete()

    return redirect(dashboard)

# a method that retrieve transaction of branch(in and out)
# it returns a list_of_transfer_transaction structured as transaction, direction


def retrieve_branch_in_and_out_transactions(branch_id):
    list_of_transfer_transaction = []
    list_of_transfer_transaction_out = Transaction.objects.filter(author_id=branch_id)  # transaction made from this branch

    for transaction in list_of_transfer_transaction_out:
        # if transaction.type == 'Ravitaillement':
        #     list_of_transfer_transaction.append( (transaction, 'in'))
        # else:
        #     list_of_transfer_transaction.append( (transaction, 'out'))
        list_of_transfer_transaction.append((transaction, 'out'))

    # let's also add trasactions to this branch
    all_computations = Transfer_Computation.objects.all()

    for computation in all_computations:
        branchTO = Branch.objects.get(id=branch_id)
        if computation.branch_to == branchTO.name:
            # get the transfer
            try:
                tr = Transfer.objects.get(computationDetails_id=computation.id)
                t = Transaction.objects.get(id=tr.transactionDetails_id)
                list_of_transfer_transaction.append((t, 'in'))
            except:
                pass

    # sorted(student_objects, key=lambda student: student.age)
    # Sort by date descending , reverse=True
    return list_of_transfer_transaction#sorted(list_of_transfer_transaction, key=lambda x: x[0].time)


def transaction_details_edit_cancel(request, tr_id):
    request.session['transfer_edit_mode'] = False
    redirect_url = reverse('transaction_details', kwargs={"tr_id": tr_id})
    return HttpResponseRedirect(redirect_url)


def transaction_details(request, tr_id):
    transfer = Transfer.objects.get(id=tr_id)
    transfer_status = "En attente"

    all_branches = get_branches(request)

    try:
        sender = Client.objects.get(id=transfer.sender_id)
        receiver = Client.objects.get(id=transfer.receiver_id)
    except Client.DoesNotExist:
        receiver = None

    computations = Transfer_Computation.objects.get(
        id=transfer.computationDetails_id)
    computations.amount = to_readable(computations.amount)
    computations.total_if_rate = to_readable(computations.total_if_rate)

    transaction = Transaction.objects.get(id=transfer.transactionDetails_id)
    # transaction.time = utc2local(transaction.time)
    # transaction.save()

    if(transfer.isCollected):
        transfer_status = "Payee"
    # transaction computation
    # computations = Transfer_Computation.objects.filter(transfer__computationDetails = transfer.computationDetails)
    author_currency = Branch.objects.get(id=transaction.author_id).currency
    author_branch = Branch.objects.get(id=transaction.author_id).name

    return render(request, 'app/Transfer/summary.html', {'author_branch': author_branch, 'sender': sender, 'author_currency': author_currency, 'all_branches': all_branches, 'receiver': receiver, 'transaction': transaction, 'computations': computations, 'transfer': transfer, 'transfer_status': transfer_status})


def transaction_details_edit(request, tr_id):
    # computations = Transfer_Computation.objects.get(id=transfer.computationDetails_id)
    # request.session['computation_id'] = computations.id

    if request.method == 'POST':
        client_receiver = Client.objects.get(
            id=Transfer.objects.get(id=tr_id).receiver_id)
        client_sender = Client.objects.get(
            id=Transfer.objects.get(id=tr_id).sender_id)

        if request.POST.get('client_receiver_surname', ''):
            client_receiver.surname = request.POST.get(
                'client_receiver_surname', '')

        if request.POST.get('client_receiver_names', '') != '':
            client_receiver.names = request.POST.get(
                'client_receiver_names', '')

        if request.POST.get('client_receiver_contactNumber', '') != '':
            client_receiver.contactNumber = request.POST.get(
                'client_receiver_contactNumber', '')

        if request.POST.get('client_sender_surname', ''):
            client_sender.surname = request.POST.get(
                'client_sender_surname', '')

        if request.POST.get('client_sender_names', '') != '':
            client_sender.names = request.POST.get('client_sender_names', '')

        if request.POST.get('client_sender_contactNumber', '') != '':
            client_sender.contactNumber = request.POST.get(
                'client_sender_contactNumber', '')

        if request.POST.get('Branch', '') != '':
            computation = Transfer_Computation.objects.get(
                id=Transfer.objects.get(id=tr_id).computationDetails_id)
            computation.branch_to = request.POST.get('Branch', '')
            computation.save()

        client_receiver.save()

        request.session['transfer_edit_mode'] = False

    else:
        request.session['transfer_edit_mode'] = True

    redirect_url = reverse('transaction_details', kwargs={"tr_id": tr_id})
    return HttpResponseRedirect(redirect_url)

# ########################## Transaction/Transfer History ##########################


def branch_transfer_history(request, branch_id):
    request.session["transaction_history_period_error"] = False
    years = []
    days = []  # Days where any transaction was mdae on this specific branch
    # If selcted, will show all the trasancation of the month
    days.append("Tout")

    list_of_transfer_transaction = retrieve_branch_in_and_out_transactions(
        branch_id)

    for tr, direction in list_of_transfer_transaction:
        if tr.time.year not in years:
            years.append(tr.time.year)

        # Compute the days
        if str(tr.time.day) not in days:
            days.append(str(tr.time.day))

    refined_list = []

    branch_to_treat = Branch.objects.get(id=branch_id)
    total_obtained_in = 0.0
    total_obtained_out = - 0.0
    balance_ouverture = 0.0
    balance_fermeture = 0.0
    status = "En attente"

    if request.method == "POST":
        if request.POST.get("year", "") == '' or request.POST.get("month", "") == '' or request.POST.get("day", "") == '':
            request.session["transaction_history_period_error"] = True
        else:
            request.session["transaction_history_period_error"] = False
            request.session['show_transfer_table'] = True

            for tr, direction in list_of_transfer_transaction:
                # if tr.type == 'Transfer':
                if str(tr.time.year) == request.POST.get("year", ""):
                    if str(tr.time.month) == request.POST.get("month", ""):
                        # Add depending on the day selected. If All, then we just append every transaction, else, we compute
                        if request.POST.get("day", "") == 'Tout' or str(tr.time.day) == request.POST.get("day", ""):
                            year = int(request.POST.get("year", ""))
                            month = int(request.POST.get("month", ""))

                            if request.POST.get("day", "") != 'Tout':
                                # balance_ouverture will be of the day before
                                day = int(request.POST.get("day", "")) - 1

                            else:
                                month = month - 1
                                # get the last day of the computed month
                                _range = monthrange(year, month)
                                day = _range[1]

                            date_to_fetch_ouverture = datetime(
                                year, month, day)
                            try:
                                balance_record_string = Balance_Record.objects.get(
                                    date=date_to_fetch_ouverture).record
                                string_to_find = '({}:'.format(
                                    branch_to_treat.id)
                                index = balance_record_string.find(
                                    string_to_find)
                                sub1 = balance_record_string[index +
                                                             len(string_to_find):]
                                i = sub1.find(')')
                                sub1 = sub1[:i]
                                balance_ouverture = float(sub1)
                            except Balance_Record.DoesNotExist:
                                balance_ouverture = 0.0
                                # return HttpResponse('Hah!')

                            if tr.type == 'Transfer':
                                le_type = tr.id
                                transfer_object = Transfer.objects.get(
                                    transactionDetails_id=tr.id)

                                # Compute the status
                                if transfer_object.isCollected == True:
                                    status = "Payee"
                                else:
                                    status = "En attente"

                                tr_computation = Transfer_Computation.objects.get(
                                    id=transfer_object.computationDetails_id)
                                if direction == 'out':
                                    refined_list.append((tr.time, transfer_object.reference, tr_computation.branch_to, tr_computation.amount,
                                                         tr_computation.total_to_pay - tr_computation.amount, tr_computation.total_to_pay, direction, status, transfer_object.id))
                                else:
                                    refined_list.append((tr.time, transfer_object.reference, tr_computation.branch_to,
                                                         0.00, 0.00, 0 - tr_computation.total_if_rate, direction, status, transfer_object.id))

                                if direction == 'in':
                                    total_obtained_in += 0 - tr_computation.total_if_rate
                                else:
                                    total_obtained_out += tr_computation.total_to_pay

                            elif tr.type == 'Expense':
                                expense = Expense.objects.get(
                                    transactionDetails_id=tr.id)
                                refined_list.append(
                                    (tr.time, expense.type, 'N/A', 0.00, 0.00, 0 - expense.amount, direction, 'N/A', '#'))

                                total_obtained_out += 0 - expense.amount

                            elif tr.type == 'Ravitaillement':
                                refueling = Refueling.objects.get(
                                    transactionDetails_id=tr.id)
                                refined_list.append(
                                    (tr.time, 'Ravitaillement', 'N/A', 0.00, 0.00, refueling.amount, direction, 'N/A', '#'))

                                total_obtained_out += refueling.amount

                        # try:
                        #     #Add depending on the day selected. If All, then we just append every transaction, else, we compute
                        #     if request.POST.get("day","") == 'Tout' or str(tr.time.day) == request.POST.get("day",""):
                        #         transfer_object = Transfer.objects.get(transactionDetails_id=tr.id)

                        #         if tr.type == 'Transfer':
                        #             # Compute the status
                        #             if transfer_object.isCollected == True:
                        #                 status = "Payee"
                        #             else:
                        #                 status = "En attente"

                        #             tr_computation = Transfer_Computation.objects.get(id = transfer_object.computationDetails_id )
                        #             if direction == 'in':
                        #                 refined_list.append((tr.time, transfer_object.reference, tr_computation.branch_to, tr_computation.amount, tr_computation.total_to_pay -  tr_computation.amount, tr_computation.total_to_pay, direction, status))
                        #             else:
                        #                 refined_list.append((tr.time, transfer_object.reference, tr_computation.branch_to, 0.00, 0.00, 0 - tr_computation.amount, direction, status))

                        #         elif tr.type == 'Expense':
                        #             expense = Expense.objects.get(transactionDetails_id=tr.id)
                        #             refined_list.append((tr.time, 'N/A', 'N/A', 0.00, 0.00, 0 - expense.amount, direction, 'N/A'))

                        #         if direction == 'in':
                        #             total_obtained_in += tr_computation.total_to_pay
                        #         else:
                        #             total_obtained_out += 0 - tr_computation.amount

                        # except:
                        #     return redirect(history_transfer)

            # table transfer containing custom columns
            balance_fermeture = balance_ouverture + \
                (total_obtained_in + total_obtained_out)

    else:
        request.session['show_transfer_table'] = False

    return render(request, 'app/settings/branch_transfer_history.html', {'balance_ouverture': balance_ouverture, 'balance_fermeture': balance_fermeture, 'days': days, 'total_obtained_out': total_obtained_out, 'total_obtained_in': total_obtained_in, 'list_of_transfer_transaction': list_of_transfer_transaction, 'refined_list': refined_list, 'branch_to_treat': branch_to_treat, 'years': years})


def history_transfer(request):
    # For stats purposes
    # Get list of branches and the number of transfer they did
    all_branches = Branch.objects.exclude(name='nat_development')
    branch_and_transfer_count = []

    for branch in all_branches:
        # transactions = retrieve_branch_in_and_out_transactions(branch.id)
        # transactions_x = []

        # for trans,dir in transactions:
        #     if trans.type == 'Transfer':
        #         transactions_x.append([trans,dir])

        # transactions = transactions_x
        # # compute in and out
        # t_in = 0
        # t_out = 0
        # tr_paid = 0

        # for tr, direction in transactions:
        #     if direction == 'in':
        #         t_in = t_in + 1

        #         # we check if has been paid and increment if necessary the tr_paid variable
        #         if tr.type == 'Transfer':
        #             transfer = Transfer.objects.get(
        #                 transactionDetails_id=tr.id)
        #             if transfer.isCollected == 1:
        #                 tr_paid = tr_paid + 1

        #         else:  # if it's one of those whoc aren't transfers, then it's already paid
        #             tr_paid = tr_paid + 1

        #     else:
        #         t_out = t_out + 1

        # branch_and_transfer_count.append((branch.name, branch.id, len(transactions), t_in, t_out, tr_paid))
        branch_and_transfer_count.append((branch.name, branch.id, 1, 1, 1, 1))

    return render(request, 'app/settings/history_transfer.html', {'branch_and_transfer_count': branch_and_transfer_count, })

# ########################## Receive Money ##########################


def transfer_receive(request):
    ################################## Security checks ##################################
    if 'current_branch_id' not in request.session:
        return redirect(logout)
    else:
        branch = Branch.objects.get(
            id=request.session.get('current_branch_id'))
        if branch.isLoggedIn == False:
            return redirect(logout)

    #######################################################################################

    if request.method == 'POST':
        try:
            transfer = Transfer.objects.get(
                reference=request.POST.get('ref_number', ''))

            # We verify if the current branch is the one suposed to collect this transfer
            # if not, we tell it nothing found

            if Transfer_Computation.objects.get(id=transfer.computationDetails_id).branch_to == request.session.get('current_branch_name') or Transaction.objects.get(id=transfer.transactionDetails_id).author_id == request.session.get('current_branch_id'):
                redirect_url = reverse('transaction_details', kwargs={
                                       "tr_id": transfer.id, })
                request.session['display_transfer_not_found'] = False
                return HttpResponseRedirect(redirect_url)

            elif request.session.get('current_branch_name') == 'arsene':
                request.session['transfer_edit_mode'] = True
                redirect_url = reverse('transaction_details', kwargs={
                                       "tr_id": transfer.id})
                return HttpResponseRedirect(redirect_url)
            else:
                request.session['display_transfer_not_found'] = True
        except:
            request.session['display_transfer_not_found'] = True
    else:
        request.session['display_transfer_not_found'] = False
    return render(request, 'app/Transfer/receive.html', {})


def pay_transfer(request, tr_id):
    ################################## Security checks ##################################
    if 'current_branch_id' not in request.session:
        return redirect(logout)
    else:
        branch = Branch.objects.get(
            id=request.session.get('current_branch_id'))
        if branch.isLoggedIn == False:
            return redirect(logout)

    #######################################################################################

    # we get the transfer details by the id number and we mark it as paid
    transfer = Transfer.objects.get(id=tr_id)
    transfer.isCollected = True
    transfer.save()

    # affect the current branch's balance | Money to give to the client so a -
    # Get the computation
    computation = Transfer_Computation.objects.get(
        id=transfer.computationDetails_id)

    receiving_branch = Branch.objects.get(name=computation.branch_to)
    receiving_branch.current_balance -= computation.total_if_rate
    receiving_branch.save()

    update_branch_session_vriables(
        request, request.session.get('current_branch_id'))

    redirect_url = reverse('transaction_details',
                           kwargs={"tr_id": transfer.id})
    return HttpResponseRedirect(redirect_url)

# ================================================================


# ########################## Fetch branch balance ##########################
def get_branch_balance(request):
    ################################## Security checks ##################################
    if 'current_branch_id' not in request.session:
        return redirect(logout)
    else:
        branch = Branch.objects.get(
            id=request.session.get('current_branch_id'))
        if branch.isLoggedIn == False:
            return redirect(logout)

    #######################################################################################

    request.session['current_branch_current_balance'] = to_readable(
        Branch.objects.get(id=request.session.get('current_branch_id')).current_balance)
    return redirect(dashboard)


def update_branch_session_vriables(request, id):

    branch = Branch.objects.get(pk=id)

    for attr, value in branch.__dict__.items():
        request.session['current_branch_' + attr] = value

    request.session['current_branch_current_balance'] = to_readable(
        branch.current_balance)

    request.session['current_branch_currency_symbol'] = Currency.objects.get(
        name=branch.currency).symbol


def dashboard(request):
    if 'current_branch_id' not in request.session:
        form = BranchForm()
        return redirect(home)
    else:

        # let's get the rate so we can tweak the dashboard main page
        # For each rate, we copy the current value inside a session that we will use to update the percentage
        all_rates = Rates.objects.all()
        unread_notifications = Notification.objects.filter(
            Recipient_id=request.session.get('current_branch_id'))
        return render(request, 'app/Branch/Branch.html', {'all_rates': all_rates, 'unread_notifications': unread_notifications, 'number_of_notification': len(unread_notifications), })


def logout(request):
    request.session.flush()
    return redirect(login)


def file_mismatch_balance_record(request, branch_id):
    # Create a new notification
    branch = Branch.objects.get(id=branch_id)
    fisrt = branch.isLoggedIn
    branch.isLoggedIn = False
    second = branch.isLoggedIn
    branch.save()
    title = 'Branche Bloquee: Balances inconsistantes'
    content = 'La branche de ' + branch.name + \
        ' souleve un cas d\'insconsistence entre la balance du system et celle en main. Veuillez-s\'il vous plait y pretter attention et ne seras operationelle qu\'une fois ce probleme resolu'
    new_notification = Notification(
        Title=title, Content=content, isRead=False, Author=branch, Recipient=Branch.objects.get(id=1))
    new_notification.save()
    return redirect(logout)


def get_tourism(request):
    return render(request, 'app/public/tourism.html', {})


def tourism_destination(request, dest_name):
    if dest_name == 'Vanderbijlpark':
        return render(request, 'app/public/Tourism/destination1.html', {})
    else:
        return redirect(home)


def get_import_export(request):
    return render(request, 'app/public/import_export.html', {})


def get_finance(request):
    return render(request, 'app/public/finance.html', {})


def get_academy(request):
    return render(request, 'app/public/academy.html', {})


def home(request):
    if request.method == 'POST':

        if form.is_valid():
            try:
                connecting_branch = Branch.objects.get(
                    name=form.cleaned_data['name'])
            except:
                return HttpResponse('Incorrect. Verifiez vos details de connection')
            else:
                if connecting_branch.passPhrase == form.cleaned_data['passPhrase']:
                    # We verify first if the branch has submitted the approval of its latest
                    # balance. If not, we display the validation page, else, they log in
                    update_branch_session_vriables(
                        request, connecting_branch.id)
                    if connecting_branch.isFrozen == False:
                        return redirect(dashboard)
                    else:
                        if connecting_branch.isLoggedIn == False:
                            return redirect(home)
                        else:
                            connecting_branch.current_balance = to_readable(
                                connecting_branch.current_balance)
                            return render(request, 'app/Branch/accounts_validation.html', {'branch': connecting_branch, })

                else:
                    return HttpResponse('Details Incorrect')

        else:
            return HttpResponse('Please contact administrator')

    else:
        form = BranchForm()
        return render(request, 'app/Main.html', {'form': form,})


def home2(request):
    return render(request, 'app/public/layout.html', {})


def login(request):
    form = BranchForm()
    if request.method == 'POST':
        fb_msg = ''
        form = BranchForm(request.POST)

        if form.is_valid():
            try:
                connecting_branch = Branch.objects.get(
                    name=form.cleaned_data['name'])
            except:
                fb_msg = "Branche inexistante."

            else:
                if connecting_branch.passPhrase == form.cleaned_data['passPhrase']:
                    # We treat the birthdays of clients here
                    birthday_clients = []
                    today = datetime.today()

                    for client in Client.objects.filter(branch_id=connecting_branch.id).exclude(birthdate__isnull=True):
                        if today.month == client.birthdate.month and today.day - client.birthdate.day <= 0:
                            delay = today.day - client.birthdate.day

                            if delay >= -2:
                                tileColor = '#800080'

                            if delay < -2:
                                tileColor = '#6a5acd'

                            if delay <= -5:
                                tileColor = '#ffd700'

                            birthday_clients.append(
                                (client.id, client.names, delay, tileColor, to_readable(client.worth))
                            )
                    request.session['birthday_clients'] = sorted( birthday_clients, key=lambda x: x[2], reverse=True)
                    request.session['birthday_clients_count'] = len(
                        birthday_clients)

                    # We verify first if the branch has submitted the approval of its latest
                    # balance. If not, we display the validation page, else, they log in
                    update_branch_session_vriables(
                        request, connecting_branch.id)
                    if connecting_branch.isFrozen == False:
                        return redirect(dashboard)
                    else:
                        if connecting_branch.isLoggedIn == False:
                            fb_msg = "Branche bloquee. Veuillez contactez 'administrateur"
                        else:
                            connecting_branch.current_balance = to_readable(
                                connecting_branch.current_balance)
                            return render(request, 'app/Branch/accounts_validation.html', {'branch': connecting_branch, })

                else:
                    fb_msg = "Details incorrects!"

        return render(request, 'app/login.html', {'form': form, 'fb_msg': fb_msg})

    else:
        return render(request, 'app/login.html', {'form': form})
