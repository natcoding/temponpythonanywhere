"""
Definition of urls for NATWebApp.
"""

from django.contrib import admin
from datetime import datetime
from django.conf.urls import url
import django.contrib.auth.views
from django.conf.urls import include

import app.forms
import app.views


admin.autodiscover()

# Uncomment the next lines to enable the admin:
# from django.conf.urls import include
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = [
    # Examples:
    #url(r'^dashboard/branch/$', app.views. , name=''),

    ################ For the public ###########################
    ## daily_money
    url(r'^daily_money/$', app.views.daily_money , name='daily_money'),
    url(r'^wizzard/(?P<br_id>\d+)/$', app.views.fidel_client , name='wizzard'),
    url(r'^wizzard/(?P<card_id>\d+)$', app.views.from_fidelity_fetecher , name='from_fidelity_fetecher'),

    ## Rates scrapper
    url(r'^rate_scrapper/$', app.views.rate_scrapper, name='rate_scrapper'),

    ## Quotations
    url(r'^quotation/(?P<service_name>[\w\-]+)/$', app.views.quotation_pages, name='quotation'),

    # Client stuff
    url(r'^dashboard/branch/clients/(?P<id>\d+)$', app.views.client_details , name='client_details'),
    url(r'^dashboard/branch/clients/$', app.views.branch_list_of_client , name='branch_list_of_client'),
    url(r'^dashboard/transfer/send/clients/verification/(?P<client_id>\d+)$', app.views.check_client , name='check_client'),
        url(r'^client_killer/$', app.views.client_killer , name='client_killer'),


    # Transfer
    url(r'^dashboard/history/transfer/branch/(?P<branch_id>\d+)$', app.views.balance_tracker , name='balance_tracker'),
    url(r'^dashboard/history/transfer/$', app.views.history_transfer , name='history_transfer'),
    url(r'^dashboard/transfer/pay/(?P<tr_id>\d+)$', app.views.pay_transfer , name='pay_transfer'),
    url(r'^dashboard/transfer/delete/(?P<tr_id>\d+)$', app.views.delete_transfer , name='delete_transfer'),
    url(r'^dashboard/transfer/cancel/$', app.views.cancel_transaction , name='cancel_transaction'),
    url(r'^dashboard/transfer/receive/$', app.views.transfer_receive , name='transfer_receive'),
    url(r'^dashboard/transfer/details/(?P<tr_id>\d+)/cancel_edit/$', app.views.transaction_details_edit_cancel , name='transaction_details_edit_cancel'),
    url(r'^dashboard/transfer/details/(?P<tr_id>\d+)/edit/$', app.views.transaction_details_edit , name='transaction_details_edit'),
    url(r'^dashboard/transfer/details/(?P<tr_id>\d+)/$', app.views.transaction_details , name='transaction_details'),
    url(r'^dashboard/transfer/send/save/$', app.views.save_Transfer, name = "save_Transfer"),
    url(r'^dashboard/transfer/send/sender/$', app.views.get_sender, name = "get_sender"),
    url(r'^dashboard/transfer/send/receiver/$', app.views.get_receiver , name='get_receiver'),

    #process_transaction_actor
    url(r'^dashboard/transfer/send/isClient/process_transaction_actor/(?P<client_id>\d+)/$', app.views.process_transaction_actor , name='process_transaction_actor'),
    url(r'^dashboard/transfer/send/isClient/$', app.views.isClient , name='isClient'),
    url(r'^dashboard/transfer/send/$', app.views.send_money, name = "transfer_send"),
    url(r'^dashboard/transfer/expenses/$', app.views.transfer_branch_expenses, name = "transfer_branch_expenses"),


    # Admin stuff
    url(r'^dashboard/history/origin/$', app.views.transactions_from_origin , name='transactions_from_origin'),
    url(r'^file_mismatch_balance_record/(?P<branch_id>\d+)/$', app.views.file_mismatch_balance_record , name='file_mismatch_balance_record'),
    url(r'^dashboard/branch/self_unfreez/(?P<branch_id>\d+)/$', app.views.self_unfreez , name='self_unfreez'),
    url(r'^dashboard/branch/admin_branch/unfreez_branch/(?P<branch_id>\d+)/$', app.views.unfreez_branch , name='unfreez_branch'),
    url(r'^dashboard/branch/admin_branch/freez_branch/(?P<branch_id>\d+)/$', app.views.freez_branch , name='freez_branch'),
    url(r'^dashboard/balance_tracker/$', app.views.balance_tracker, name='balance_tracker'),
    url(r'^dashboard/add_currency/$', app.views.add_currency , name='add_currency'),
    url(r'^dashboard/branch/admin_branch/$', app.views.admin_branch , name='admin_branch'),
    url(r'^dashboard/costs/(?P<id>\d+)/$', app.views.costs_delete , name='costs_delete'),
    url(r'^dashboard/costs/$', app.views.costs , name='costs'),
    url(r'^dashboard/rates/$', app.views.rates , name='rates'),
    url(r'^dashboard/branch/delete/(?P<id>\d+)/$', app.views.delete_branch , name='delete_branch'),
    url(r'^dashboard/branch/edit/(?P<id>\d+)/$', app.views.edit_branch , name='edit_branch'),
    url(r'^dashboard/branch/refuel/cancel/$', app.views.cancel_refueling , name='cancel_refueling'),
    url(r'^dashboard/branch/refuel/(?P<id>\d+)/$', app.views.refuel_branch , name='refuel_branch'),
    url(r'^dashboard/branch/add/$', app.views.add_branch , name='add_branch'),
    url(r'^dashboard/new_client/$', app.views.new_client, name='new_client'),



    # Divers
    url(r'^dashboard/logout/$', app.views.logout, name='logout'),
    url(r'^dashboard/get_branch_balance$', app.views.get_branch_balance, name='get_branch_balance'),
    url(r'^dashboard/$', app.views.dashboard, name='dashboard'),
    url(r'^rebalancer/$', app.views.rebalancer, name='rebalancer'),
    url(r'^br_login/$', app.views.login, name='login'),



    url(r'^test/$', app.views.home2, name='home2'),
    url(r'^get_started/academy/$', app.views.get_academy, name='get_academy'),

    url(r'^get_started/tourism/$', app.views.get_tourism, name='get_tourism'),
    url(r'^get_started/tourism/destination/(?P<dest_name>[\w\-]+)/$', app.views.tourism_destination, name='tourism_destination'),

    url(r'^get_started/finance/$', app.views.get_finance, name='get_finance'),
    url(r'^get_started/import&export/$', app.views.get_import_export, name='get_import_export'),
    url(r'^$', app.views.home2, name='home'),

    # Django admin
    url(r'^admin/', include(admin.site.urls)),
]
